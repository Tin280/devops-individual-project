require('dotenv').config();
const app = require('./routes')

const PORT = process.env.PORT 
console.log(PORT)
const HOSTNAME = process.env.IP_ADDRESS
app.listen(PORT,HOSTNAME , () => {
  console.log(`Listening to http://${HOSTNAME}:${PORT}`)
})