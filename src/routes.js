const express = require('express');
const shortid = require('shortid');
const app = express();
app.use(express.json());

const urlDatabase = {}; // Simulated database for URL storage

app.get('/',(req,res) => {
  res.send("Hello User")
})
app.post('/shorten', (req, res) => {
  const { url } = req.body;
  if (!url) {
    return res.status(400).json({ error: 'URL is required' });
  }

  // Check if the URL already exists in the database
  const existingShortUrl = Object.keys(urlDatabase).find((shortUrl) => urlDatabase[shortUrl] === url);

  if (existingShortUrl) {
    return res.json({ shortUrl: existingShortUrl, originalUrl: url });
  }

  const shortUrl = shortid.generate();
  urlDatabase[shortUrl] = url;
  console.log("List of URL",urlDatabase)
  console.log("Full URL",url)

  res.json({ shortUrl, originalUrl: url });
});

app.get('/:shortUrl', (req, res) => {
  const { shortUrl } = req.params;
  const originalUrl = urlDatabase[shortUrl];

  if (originalUrl) {
    return res.redirect(originalUrl);
  }

  res.status(404).json({ error: 'URL not found' });
});

module.exports = app;